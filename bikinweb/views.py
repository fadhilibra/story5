from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Schedule
from . import forms


# Create your views here.
def home(request):
    return render(request, 'website-gue.html')

def experiences(request):
    return render(request, 'experiences.html')

def skills(request):
    return render(request, 'skills.html')

def education(request):
    return render(request, 'education.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")

def delete(request,delete_id):
    Schedule.objects.filter(id=delete_id).delete()
    return redirect('schedule')


           
    