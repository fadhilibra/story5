from django.urls import path
from django.conf.urls import url
from . import views
from .views import *

urlpatterns = [
    url(r"^deleteurl/(?P<delete_id>\d+)/$",delete, name='delete'),
    path('', views.home, name = 'home'),
    path('experiences/', views.experiences, name = 'experiences'),
    path('aboutme/', views.aboutme, name = 'aboutme'),
    path('skills/', views.skills, name = 'skills'),
    path('education/', views.education, name = 'education'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),

    
]






